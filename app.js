document.addEventListener("DOMContentLoaded", function() {
    
    // Get DOM elements
    const laptopsElement = document.getElementById('laptops');
    const laptopSpecs = document.getElementById('laptop-specs-list');
    const bankerBalanceDiv = document.getElementById('balance');
    const loanRequestAmount = document.getElementById('loan-request-amount');
    const bankerLoanContainer = document.getElementById('banker-loan-container');
    const repayLoanButton = document.getElementById('repay-loan-button');
    const pay = document.getElementById('pay');
    const loan = document.getElementById('loan');
    const laptopTitle = document.getElementById('laptop-title');
    const laptopInformation = document.getElementById('laptop-information');
    const laptopDescription = document.getElementById('laptop-description');
    const laptopImg = document.getElementById('laptop-img');
    const laptopPrice = document.getElementById('laptop-price');
    const loanModal = document.getElementById('loan-modal');

    // Initialize variables
    let laptops = [];
    let balance = 200;
    let salary = 0;
    let openLoan = 0;
    let selectedLaptop = null;
    let additionalSalary = 0;

    // Set starting balance
    function setStartingBalance() {
        bankerBalanceDiv.textContent = balance;
    }

    // Validate loan request
    function loanValidation() {
        const requestedBalance = parseInt(loanRequestAmount.value);
        loanModal.classList.remove('modal-visible');

        if (bankerLoanContainer.classList.contains('has-loan')) {
            alert("You can't have more than one loan. Please pay back previous loan and try again");
            return false;
        }
        if (!Number.isInteger(requestedBalance)) {
            alert('Please enter a valid number');
            return false;
        }
        if (requestedBalance > balance * 2) {
            alert("Your loan can't be more than twice your current balance");
            return false;
        }
    
        alert('Your loan has been approved. The amount has been added to your balance');
        bankerLoanContainer.classList.add('has-loan');
        repayLoanButton.classList.add('has-loan');
        balance += requestedBalance;
        openLoan = requestedBalance;
        bankerBalanceDiv.textContent = balance;
        loan.textContent = requestedBalance;
    }

    // Add work salary
    function addWorkSalary() {
        salary += 100;
        pay.textContent = salary;
    }

    // Check if loan is paid off
    function checkLoanPaidOff() {
        if (openLoan === 0) {
            bankerLoanContainer.classList.remove('has-loan');
            repayLoanButton.classList.remove('has-loan');
            alert('Congratulations! You paid off your loan!');
        }
    }
  

    // Transfer money to bank
    function transferToBank() {
        if (openLoan > 0) {
            const loanPayment = salary / 10;
            salary -= loanPayment;
            openLoan -= loanPayment;
            if(openLoan < 0){
                additionalSalary = Math.abs(openLoan);
                balance += additionalSalary;
                openLoan = 0;
            }
            loan.textContent = openLoan;
            checkLoanPaidOff();
        }
        balance += salary;
        bankerBalanceDiv.textContent = balance;
        pay.textContent = 0;
        salary = 0;
        additionalSalary = 0;
    }

    // Repay loan
    function repayLoan() {
        openLoan -= salary;
        if (openLoan < 0) {
            salary = Math.abs(openLoan);
            balance += salary;
            openLoan = 0;
            bankerBalanceDiv.textContent = balance;
        }
        loan.textContent = openLoan;
        checkLoanPaidOff();
        pay.textContent = 0;
        salary = 0;
    }

    fetch("https://hickory-quilled-actress.glitch.me/computers")
        .then(response => response.json())
        .then(data => laptops = data)
        .then(laptops => addLaptopsToMenu(laptops));

    // Add laptops to menu
    function addLaptopsToMenu(laptops) {
        laptops.forEach(addLaptopToMenu);
    }

    // Add a laptop to the menu
    function addLaptopToMenu(laptop) {
        const laptopElement = document.createElement('option');
        laptopElement.value = laptop.id;
        laptopElement.textContent = laptop.title;
        laptopsElement.appendChild(laptopElement);
    }

    // Handle laptop change event
    const handleLaptopChange = () => {
        selectedLaptop = laptops[laptopsElement.selectedIndex];
        refreshLaptopFeatures(selectedLaptop);
    }    

    const refreshLaptopFeatures = (laptop) => {
        const features = laptop.specs;
        const laptopFeatures = features.map((feature) => `<li class="laptop-feature">${feature}</li>`).join('');
        laptopSpecs.innerHTML = laptopFeatures;
        laptopTitle.textContent = laptop.title;
        laptopInformation.classList.add('is-visible');
        laptopDescription.textContent = laptop.description;
        laptopPrice.textContent = laptop.price;
        const imageUrl = `https://hickory-quilled-actress.glitch.me/${laptop.image}`;
        loadLaptopImage(imageUrl);
    };

    // Fetch image for laptop
    const loadLaptopImage = async (url) => {
        const backupImage = 'https://hickory-quilled-actress.glitch.me/assets/images/1.png';
      
        try {
            const response = await fetch(url, { method: 'GET' });
            if (response.ok) {
                console.log('Response ok');
                setLaptopImage(url);
            } else {
                console.log('Response not ok');
                const altUrl = url.replace('.jpg', '.png');
                try {
                const altResponse = await fetch(altUrl, { method: 'GET' });
                if (altResponse.ok) {
                    console.log('Alt url response ok');
                    setLaptopImage(altUrl);
                } else {
                    console.log('Alt url response not ok');
                }
                } catch (error) {
                console.error(error);
                }
            }
        } catch (error) {
          console.error(error);
        }
    };

    //Handler for buying laptop
    const handleLaptopBuy = () => {
        if (balance < selectedLaptop['price']) {
          alert('Not enough money to purchase this laptop.');
        } else {
          balance -= selectedLaptop['price'];
          bankerBalanceDiv.textContent = balance;
          alert('Purchase accepted!');
        }
    };
    
    // Set laptop image
    const setLaptopImage = (url) => {
        laptopImg.src = url;
    };

    // Event listeners
    window.addEventListener('load', setStartingBalance);
    laptopsElement.addEventListener('change', handleLaptopChange);
    document.getElementById('loan-button').addEventListener('click', () => loanModal.classList.add('modal-visible'));
    document.getElementById('loan-request-button').addEventListener('click', loanValidation);
    document.getElementById('work-button').addEventListener('click', addWorkSalary);
    document.getElementById('bank-button').addEventListener('click', transferToBank);
    document.getElementById('repay-loan-button').addEventListener('click', repayLoan);
    document.getElementById('laptop-pay-button').addEventListener('click', handleLaptopBuy);
});