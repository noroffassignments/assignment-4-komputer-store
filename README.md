# Komputer Store

This is an assignment from Noroff for the Komputer store.

## Features

- Displaying available laptops for sale
- Buying laptops
- Checkout process
- Loan request feature
- Loan repayment feature

## Technologies Used

- HTML
- CSS
- JavaScript

## Installation and Usage

1. Clone the repository to your local machine.
2. Open the project in your web browser.

## Authors

Niels van Mierlo https://gitlab.com/NVM238